import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './external/login/login.component';
import { RegistrationComponent } from './external/registration/registration.component';
import {ExternalAuthguardService} from './services/externalAuthguard.service';
import { ForgotPasswordComponent } from './external/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './external/reset-password/reset-password.component';

const routes: Routes = [
    {path: '', redirectTo: '/login', pathMatch: 'full'},
    // {path: '**', redirectTo: '/login', pathMatch: 'full'},
    {path: 'login', component: LoginComponent, canActivate: [ExternalAuthguardService]},
    {path: 'register', component: RegistrationComponent},
    {path: 'forgot-password', component: ForgotPasswordComponent},
    {path: 'reset-password', component: ResetPasswordComponent},
    {path: '', loadChildren: () => import('./internal/internal.module').then(m => m.InternalModule)}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {
}
