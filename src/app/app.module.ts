import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './external/login/login.component';
import { RegistrationComponent } from './external/registration/registration.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { OnlyNumber } from './_helpers/onlynumber.directive';
import { ReactiveFormsModule } from '@angular/forms';
import {MatStepperModule} from '@angular/material/stepper';
import {MatFormFieldModule} from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { ForgotPasswordComponent } from './external/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './external/reset-password/reset-password.component';
import { ForgotPasswordSendlinkComponent } from './popup/forgot-password-sendlink/forgot-password-sendlink.component';
import {MatDialogModule} from '@angular/material/dialog';
import { VerifyCheckComponent } from './popup/verify-check/verify-check.component';
import { RefillFormComponent } from './popup/refill-form/refill-form.component';
import { ListConfirmationComponent } from './popup/list-confirmation/list-confirmation.component';
import { AddVariationsComponent } from './popup/add-variations/add-variations.component';
import { LinkPhotosComponent } from './popup/link-photos/link-photos.component';
import {MatExpansionModule} from '@angular/material/expansion';
import { AllOrderDetailsComponent } from './popup/all-order-details/all-order-details.component';
import { CancelOrderComponent } from './popup/cancel-order/cancel-order.component';
import {MatRadioModule} from '@angular/material/radio';
import { AddSocialLinkComponent } from './popup/add-social-link/add-social-link.component';
import { InterceptorService } from './services/interceptor.service';
import { ExternalAuthguardService } from './services/externalAuthguard.service';
import { HttpService } from './services/http.service';
import { ToastrModule } from 'ngx-toastr';
import { ImageProcessingService } from './services/image-processing-service';
import {  MatSelectModule } from '@angular/material/select';
import { DragDirective } from './_helpers/dragDrop.directive';
import { CrystalLightboxModule } from '@crystalui/angular-lightbox';
import { BsDatepickerModule, DatepickerModule } from 'ngx-bootstrap/datepicker';
import {MatAutocompleteModule} from '@angular/material/autocomplete';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    OnlyNumber,
    DragDirective,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    ForgotPasswordSendlinkComponent,
    VerifyCheckComponent,
    RefillFormComponent,
    ListConfirmationComponent,
    AddVariationsComponent,
    LinkPhotosComponent,
    AllOrderDetailsComponent,
    CancelOrderComponent,
    AddSocialLinkComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    MatStepperModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatDialogModule,
    MatExpansionModule,
    MatRadioModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    MatSelectModule,
    CrystalLightboxModule,
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    MatAutocompleteModule
    
   
    
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    },
    HttpService,
    ExternalAuthguardService,
    ImageProcessingService
  
     

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }