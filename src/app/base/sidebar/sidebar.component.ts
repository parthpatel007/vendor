import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  show:boolean;
  text:string;

  constructor(private _router: Router) { }

  ngOnInit(): void {
    this.show=true;
  }

  //onclick
toggleMenu() {
 this.show=this.show==false?true:false;

 }

  logout(){
    localStorage.removeItem('accessToken');
    this._router.navigate(['/login'])
  }

}
