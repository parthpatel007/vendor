import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiUrl } from 'src/app/services/apiurl';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  forgotForm: any = FormGroup
  submitted = false;
  button = 'Send OTP';
  isLoading = false;

  constructor(private formBuilder: FormBuilder, private _router: Router, public http: HttpService, private toastr: ToastrService) {
    this.forgotForm = this.formBuilder.group({
      mobile: ['', Validators.required]
    })
  }


  ngOnInit(): void {
  }

  get f() { return this.forgotForm.controls; }

  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.forgotForm.invalid) {
      return;
    }

    this.isLoading = true;
    this.button = 'Processing';

    let payload = {
      mobileno: this.forgotForm.value.mobile,
    }

    this.http.postData(ApiUrl.sendOtp, payload)
      .subscribe(res => {
        if (res.success_code == 200) {
          this.isLoading = false;
          this.button = 'Send OTP';
          this.toastr.success(res.message, 'success', {
            timeOut: 2000
          });
          this._router.navigate(['/reset-password'], { queryParams: { mobile: this.forgotForm.value.mobile } });
        }
      },
        () => {

          this.isLoading = false;
          this.button = 'Send OTP';

        });

  }
  keyDownFunction(event: any) {
    if (event.keyCode === 13) {
      this.onSubmit();
    }
  }
}
