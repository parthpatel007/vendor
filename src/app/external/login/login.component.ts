import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiUrl } from 'src/app/services/apiurl';
import { HttpService } from 'src/app/services/http.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: any = FormGroup
  submitted = false;
  password: string = 'password';
  button = 'Sign in';
  isLoading = false;

  constructor(private formBuilder: FormBuilder, private _router: Router, public http: HttpService, private toastr: ToastrService) {
    this.loginForm = this.formBuilder.group({
      mobile: ['', Validators.required],
      password: ['', Validators.required],
      rememberme: ['']
    })
  }

  ngOnInit(): void {
    this.password = 'password';
    this.loadScript('../../../assets/js/slider.js');
    if (localStorage.getItem('rememberme') == 'yes') {
      this.loginForm.controls.mobile.setValue(localStorage.getItem('mobile'))
      this.loginForm.controls.password.setValue(localStorage.getItem('password'))
    }
  }
  onClickHide() {
    if (this.password === 'password') {
      this.password = 'text';
    } else {
      this.password = 'password';
    }
  }
  get f() { return this.loginForm.controls; }


  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.isLoading = true;
    this.button = 'Processing';

    let payload = {
      mobileno: this.loginForm.value.mobile,
      password: this.loginForm.value.password,

    }

    this.http.postData(ApiUrl.login, payload)
      .subscribe(res => {
        if (res.success_code == 200) {
          localStorage.setItem('accessToken', res.data.token);
          if (this.loginForm.value.rememberme) {
            localStorage.setItem('rememberme', "yes");
            localStorage.setItem('mobile', this.loginForm.value.mobile);
            localStorage.setItem('password', this.loginForm.value.password);
          }
          this.isLoading = false;
          this.button = 'Sign in';
          this.toastr.success(res.message, 'success', {
            timeOut: 2000
          });
          window.location.href = "dashboard";

        }
      },
        () => {

          this.isLoading = false;
          this.button = 'Sign in';

        });

  }

  keyDownFunction(event: any) {
    if (event.keyCode === 13) {
      this.onSubmit();
    }
  }

  public loadScript(url: string) {
    const body = <HTMLDivElement>document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }


}
