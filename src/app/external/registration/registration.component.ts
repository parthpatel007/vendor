import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatHorizontalStepper } from '@angular/material/stepper';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiUrl } from 'src/app/services/apiurl';
import { HttpService } from 'src/app/services/http.service';
import { ImageProcessingService } from 'src/app/services/image-processing-service';
import * as constant from '../../services/constants';
import { FileHandle } from '../../_helpers/dragDrop.directive';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  @ViewChild('stepper') stepper: any = MatHorizontalStepper;
  isLinear = false;
  firstFormGroup: any = FormGroup;
  secondFormGroup: any = FormGroup;
  thirdFormGroup: any = FormGroup;
  fourthFormGroup: any = FormGroup;
  password: string = 'password';
  isEditable = false;
  error = true;
  submitted = false;
  submitted1 = false;
  submitted2 = false;
  submitted3 = false;
  otpButton = 'Send OTP'
  otpVisible = true
  lengthMobile: any
  mobileNumber: any
  disablePassword: boolean = true;
  display: any;
  disableField = true;
  rightCheck = false
  imageFile: any
  imageUrl: any
  disabledField = false;
  crossCheck = false
  pincode: any = []
  pincodeButton: any = 'Enter Pincode'
  state: any = []
  states = constant.state
  stateButton: any = 'Enter State'
  pincodeclass = false
  checkFile: any
  checkUrl: any
  files: FileHandle[] = [];
  files1: FileHandle[] = [];
  files2: FileHandle[] = [];
  files3: FileHandle[] = [];
  files4: FileHandle[] = [];
  adharCardFile: any
  adharCardBackFile: any
  adharCardUrl: any
  adharBackUrl: any
  hideResendButton = false
  hideSendButton = true
  checked: boolean = false;
  public: boolean = false
  profileImage: any
  checkImage: any
  adharImage: any
  adharBackImage: any
  adharProofUrl: any
  adharProofFile: any
  adharProofImage: any
  IPAddress:any
  hideOtpInput=false
  newVar:Date


  constructor(private _formBuilder: FormBuilder, private _router: Router, public http: HttpService, private toastr: ToastrService, public changeDetect: ChangeDetectorRef, private ImageService: ImageProcessingService) { }
  ngOnInit() {
    this.newVar = new Date();
    this.loadScript('../../../assets/js/slider.js');
    this.firstFormGroup = this._formBuilder.group({
      mobile: ['', Validators.required],
      email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      password: ['', [Validators.required]],
      vendorName: ['', Validators.required],
      shopName: ['', Validators.required],
      agree: ['', Validators.requiredTrue],
    });
    this.secondFormGroup = this._formBuilder.group({
      gstNo: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      bankName: ['', Validators.required],
      accountNo: ['', Validators.required],
      ifscCode: ['', Validators.required],
      accountHolderName: ['', Validators.required]
    });
    this.fourthFormGroup = this._formBuilder.group({
      adharName: ['', Validators.required],
      adharCardNumber: ['', Validators.required],
      adharCardDOB: ['', Validators.required],
    });
    this.password = 'password';
    this.getIpAddress()
  }
  ngAfterViewInit() {
    this.stepper._getIndicatorType = () => 'number';
  }
  onClickHide() {
    if (this.password === 'password') {
      this.password = 'text';
    } else {
      this.password = 'password';
    }
  }

  keyup(event: any) {
    this.lengthMobile = `${event.target.value}`.length
    this.mobileNumber = event.target.value
  }
  keyup1(event: any) {
    if (this.disabledField) {
      return
    }
    let otp = `${event.target.value}`.length
    if (otp === 4) {
      let payload = {
        mobileno: this.mobileNumber,
        otp: event.target.value
      }
      this.http.postData(ApiUrl.verifyOtp, payload)
        .subscribe(res => {
          if (res.success_code == 200) {
            this.rightCheck = true
            this.crossCheck = false
            this.disableField = false
            this.disabledField = true;
            this.hideSendButton = false
            this.toastr.success(res.message, 'success', {
              timeOut: 2000
            });
            setTimeout(()=>{                           // <<<---using ()=> syntax
              this.otpVisible = true;
              this.hideOtpInput = true
          }, 3000);
          } else {
            this.crossCheck = true
            this.rightCheck = false
            this.toastr.error(res.message, 'error', {
              timeOut: 2000
            })
          }
        },
          () => {
          });
    }
  }


  verifyMobile(otpButton: any) {

    if (otpButton == 'Send OTP') {
      this.otpButton = 'Resend OTP'
      let payload = {
        mobileno: this.mobileNumber
      }
      this.http.postData(ApiUrl.varifyMobile, payload)
        .subscribe(res => {
          if (res.success_code == 200) {
            this.http.postData(ApiUrl.sendOtp, payload)
              .subscribe(res => {
                if (res.success_code == 200) {
                  this.disablePassword = false
                  this.otpVisible = false
                  this.toastr.success(res.message, 'success', {
                    timeOut: 2000
                  });
                }
              },
                () => {
                });

          }
          else {
            this.toastr.error(res.message, 'error', {
              timeOut: 2000
            })
          }
        },
          () => {
          });
    } else {
      this.disabledField = true;
      var minute = 1
      let seconds: number = minute * 30;
      let textSec: any = "0";
      let statSec: number = 30;

      const prefix = minute < 10 ? "0" : "";

      const timer = setInterval(() => {
        seconds--;
        if (statSec != 0) statSec--;
        else statSec = 59;

        if (statSec < 10) {
          textSec = "0" + statSec;
        } else textSec = statSec;

        this.display = `${prefix}${Math.floor(seconds / 60)}:${textSec}`;

        if (seconds == 0) {
          console.log("finished");
          this.resendotp()
          clearInterval(timer);
        }
      }, 1000);
      this.otpButton = 'Resend OTP in '
      this.hideResendButton = true

    }
  }

  resendotp() {
    let payload = {
      mobileno: this.mobileNumber
    }
    this.http.postData(ApiUrl.varifyMobile, payload)
      .subscribe(res => {
        if (res.success_code == 200) {
          this.http.postData(ApiUrl.sendOtp, payload)
            .subscribe(res => {
              if (res.success_code == 200) {
                this.disablePassword = false
                this.otpVisible = false
                this.hideResendButton = false
                this.toastr.success(res.message, 'success', {
                  timeOut: 2000
                });
              }
            },
              () => {
              });

        }
        else {
          this.toastr.error(res.message, 'error', {
            timeOut: 2000
          })
        }
      },
        () => {
        });
  }

  public imageVideo(): void {
    this.ImageService.imageFile().then((image: File) => {
      this.imageFile = image
      if (image.size >= 5000000) {
        this.toastr.error('File too Big, please select a file less than 5mb', 'error', {
          timeOut: 2000
        })
        return
      }
      var reader = new FileReader();
      reader.readAsDataURL(image);
      reader.onload = (_event) => {
        this.imageUrl = reader.result
      }
      var form_data = new FormData();

      form_data.append('img', this.imageFile);

      this.http.postData(ApiUrl.uploadImage, form_data)
        .subscribe(res => {
          if (res.success_code == 200) {
            this.profileImage = res.data
            console.log(this.profileImage)
            this.toastr.success(res.message, 'success', {
              timeOut: 2000
            });
          }
        },
          () => {
          });

    })
  }

  public checkImages(): void {
    this.ImageService.imageFile().then((image: File) => {
      this.checkFile = image
      if (image.size >= 5000000) {
        this.toastr.error('File too Big, please select a file less than 5mb', 'error', {
          timeOut: 2000
        })
        return
      }
      var reader = new FileReader();
      reader.readAsDataURL(image);
      reader.onload = (_event) => {
        this.checkUrl = reader.result
      }
      var form_data = new FormData();

      form_data.append('img', this.checkFile);

      this.http.postData(ApiUrl.uploadImage, form_data)
        .subscribe(res => {
          if (res.success_code == 200) {
            this.checkImage = res.data
            console.log(this.checkImage)
            this.toastr.success(res.message, 'success', {
              timeOut: 2000
            });
          }
        },
          () => {
          });
    })
  }

  public adharImages(): void {
    this.ImageService.imageFile().then((image: File) => {
      this.adharCardFile = image
      if (image.size >= 5000000) {
        this.toastr.error('File too Big, please select a file less than 5mb', 'error', {
          timeOut: 2000
        })
        return
      }
      var reader = new FileReader();
      reader.readAsDataURL(image);
      reader.onload = (_event) => {
        this.adharCardUrl = reader.result
      }
      var form_data = new FormData();

      form_data.append('img', this.adharCardFile);

      this.http.postData(ApiUrl.uploadImage, form_data)
        .subscribe(res => {
          if (res.success_code == 200) {
            this.adharImage = res.data
            this.toastr.success(res.message, 'success', {
              timeOut: 2000
            });
          }
        },
          () => {
          });
    })
  }

  public adharBackImages(): void {
    this.ImageService.imageFile().then((image: File) => {
      this.adharCardBackFile = image
      if (image.size >= 5000000) {
        this.toastr.error('File too Big, please select a file less than 5mb', 'error', {
          timeOut: 2000
        })
        return
      }
      var reader = new FileReader();
      reader.readAsDataURL(image);
      reader.onload = (_event) => {
        this.adharBackUrl = reader.result
      }
      var form_data = new FormData();

      form_data.append('img', this.adharCardBackFile);

      this.http.postData(ApiUrl.uploadImage, form_data)
        .subscribe(res => {
          if (res.success_code == 200) {
            this.adharBackImage = res.data
            this.toastr.success(res.message, 'success', {
              timeOut: 2000
            });
          }
        },
          () => {
          });
    })
  }

  public adharProofImages(): void {
    this.ImageService.imageFile().then((image: File) => {
      this.adharProofFile = image
      if (image.size >= 5000000) {
        this.toastr.error('File too Big, please select a file less than 5mb', 'error', {
          timeOut: 2000
        })
        return
      }
      var reader = new FileReader();
      reader.readAsDataURL(image);
      reader.onload = (_event) => {
        this.adharProofUrl = reader.result
      }
      var form_data = new FormData();

      form_data.append('img', this.adharProofFile);

      this.http.postData(ApiUrl.uploadImage, form_data)
        .subscribe(res => {
          if (res.success_code == 200) {
            this.adharProofImage = res.data
            this.toastr.success(res.message, 'success', {
              timeOut: 2000
            });
          }
        },
          () => {
          });
    })
  }

  public loadScript(url: string) {
    const body = <HTMLDivElement>document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }

  getPincode() {
    this.http.getData(ApiUrl.pincode).subscribe((res) => {
      if (res.success_code == 200) {
        this.pincode = res.data.pincode
      }
    });
  }

  pincodes(name: any) {
    this.pincodeButton = name
    this.http.getStateByPincode(ApiUrl.getState, name).subscribe((res) => {
      this.state = res
      console.log(this.state)
      this.secondFormGroup.controls.city.setValue(this.state.PostOffice[0].District);
      this.secondFormGroup.controls.state.setValue(this.state.PostOffice[0].State);
    });
  }

  statesData(name: any) {
    this.stateButton = name;
  }


  filesDroppedProfile(files: FileHandle[]): void {
    this.files = files;
    if (this.files[0].file.size >= 5000000) {
      this.toastr.error('File too Big, please select a file less than 5mb', 'error', {
        timeOut: 2000
      })
      return;
    }
    var reader = new FileReader();
    reader.readAsDataURL(this.files[0].file);
    reader.onload = (_event) => {
      this.imageUrl = reader.result
    }


    var form_data = new FormData();

    form_data.append('img', this.files[0].file);

    this.http.postData(ApiUrl.uploadImage, form_data)
      .subscribe(res => {
        if (res.success_code == 200) {
          this.profileImage = res.data
          console.log(this.profileImage)
          this.toastr.success(res.message, 'success', {
            timeOut: 2000
          });
        }
      },
        () => {
        });

  }

  filesDropped(files: FileHandle[]): void {
    this.files1 = files;
    if (this.files1[0].file.size >= 5000000) {
      this.toastr.error('File too Big, please select a file less than 5mb', 'error', {
        timeOut: 2000
      })
      return;
    }
    var reader = new FileReader();
    reader.readAsDataURL(this.files1[0].file);
    reader.onload = (_event) => {
      this.checkUrl = reader.result
    }
    var form_data = new FormData();

    form_data.append('img', this.files1[0].file);

    this.http.postData(ApiUrl.uploadImage, form_data)
      .subscribe(res => {
        if (res.success_code == 200) {
          this.checkImage = res.data
          console.log(this.checkImage)
          this.toastr.success(res.message, 'success', {
            timeOut: 2000
          });
        }
      },
        () => {
        });
  }

  filesDroppedAdhar(files: FileHandle[]): void {
    this.files2 = files;
    if (this.files2[0].file.size >= 5000000) {
      this.toastr.error('File too Big, please select a file less than 5mb', 'error', {
        timeOut: 2000
      })
      return;
    }
    var reader = new FileReader();
    reader.readAsDataURL(this.files2[0].file);
    reader.onload = (_event) => {
      this.adharCardUrl = reader.result
    }
    var form_data = new FormData();

    form_data.append('img', this.files2[0].file);

    this.http.postData(ApiUrl.uploadImage, form_data)
      .subscribe(res => {
        if (res.success_code == 200) {
          this.adharImage = res.data
          this.toastr.success(res.message, 'success', {
            timeOut: 2000
          });
        }
      },
        () => {
        });
  }

  AadharBack(files: FileHandle[]): void {
    this.files3 = files;
    if (this.files3[0].file.size >= 5000000) {
      this.toastr.error('File too Big, please select a file less than 5mb', 'error', {
        timeOut: 2000
      })
      return;
    }
    var reader = new FileReader();
    reader.readAsDataURL(this.files3[0].file);
    reader.onload = (_event) => {
      this.adharBackUrl = reader.result
    }
    var form_data = new FormData();

    form_data.append('img', this.files3[0].file);

    this.http.postData(ApiUrl.uploadImage, form_data)
      .subscribe(res => {
        if (res.success_code == 200) {
          this.adharBackImage = res.data
          this.toastr.success(res.message, 'success', {
            timeOut: 2000
          });
        }
      },
        () => {
        });
  }

  Aadharproof(files: FileHandle[]): void {
    this.files4 = files;
    if (this.files4[0].file.size >= 5000000) {
      this.toastr.error('File too Big, please select a file less than 5mb', 'error', {
        timeOut: 2000
      })
      return;
    }
    var reader = new FileReader();
    reader.readAsDataURL(this.files4[0].file);
    reader.onload = (_event) => {
      this.adharProofUrl = reader.result
    }
    var form_data = new FormData();

    form_data.append('img', this.files4[0].file);

    this.http.postData(ApiUrl.uploadImage, form_data)
      .subscribe(res => {
        if (res.success_code == 200) {
          this.adharProofImage = res.data.originalURL
          this.toastr.success(res.message, 'success', {
            timeOut: 2000
          });
        }
      },
        () => {
        });
  }

  deleteCheck() {
    this.checkUrl = undefined
    this.checkImage = undefined
  }

  deleteAdhar() {
    this.adharCardUrl = undefined
    this.adharImage = undefined
  }

  deleteAdharBack() {
    this.adharBackUrl = undefined
    this.adharBackImage = undefined
  }

  deleteAdharProof() {
    this.adharProofUrl = undefined
    this.adharProofImage = undefined
  }

  changeValue(value: any) {
    this.checked = !value;
  }

  get f() { return this.firstFormGroup.controls; }
  get f1() { return this.secondFormGroup.controls; }
  get f2() { return this.thirdFormGroup.controls; }
  get f3() { return this.fourthFormGroup.controls; }

  firstForm() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.firstFormGroup.invalid) {
      return;
    }
    this.getPincode()
  }


  SecondForm() {

    if (this.profileImage == undefined) {
      this.toastr.error('Please Upload your profile picture ', 'error', {
        timeOut: 2000
      })
      return
    }

    this.submitted1 = true;
    // stop here if form is invalid
    if (this.secondFormGroup.invalid && this.pincodeButton == 'Enter Pincode' && this.stateButton == 'Enter State') {
      this.pincodeclass = true
      return;
    }


  }

  thirdForm() {
    if (this.checkImage == undefined) {
      this.toastr.error('Please Upload cancel check image ', 'error', {
        timeOut: 2000
      })
      return
    }
    this.submitted2 = true;
    // stop here if form is invalid
    if (this.thirdFormGroup.invalid) {
      return;
    }
  }

  fourthForm() {
    if (this.adharImage == undefined) {
      this.toastr.error('Please Upload adharcard front image ', 'error', {
        timeOut: 2000
      })
      return
    }
    if (this.adharBackImage == undefined) {
      this.toastr.error('Please Upload adharcard back image ', 'error', {
        timeOut: 2000
      })
      return
    }

    if (this.adharProofImage == undefined) {
      this.toastr.error('Please Upload addressproof image ', 'error', {
        timeOut: 2000
      })
      return
    }
    this.submitted3 = true;
    // stop here if form is invalid
    if (this.fourthFormGroup.invalid) {
      return;
    }

    this.finalSubmit()
  }

  getIpAddress(){
    this.http.getIp(ApiUrl.getIP).subscribe((res) => {
     this.IPAddress = res.IPv4
    });
  }

  // finalSubmit() {

  //   let payload =
  //   {
  //     mobileno: this.firstFormGroup.value.mobile,
  //     emailid: this.firstFormGroup.value.email,
  //     password: this.firstFormGroup.value.password,
  //     vendor_name: this.firstFormGroup.value.vendorName,
  //     vendor_description: "Nikes parter of product",
  //     "shop_cover_img": "https/otm-line/s3-bucket/vendor/shop_cover_img.jpg",
  //     vendor_profile_img: this.profileImage,
  //     "address_proof_img": "https/otm-line/s3-bucket/vendor/address_proof _img.jpg",
  //     cancel_check_img: this.checkImage,
  //     shop_name: this.firstFormGroup.value.shopName,
  //     aadhar_card: {
  //       aadhar_card_number: this.fourthFormGroup.value.adharCardNumber,
  //       DOB: this.fourthFormGroup.value.adharCardDOB,
  //       aadhar_card_name: this.fourthFormGroup.value.adharName
  //     },
  //     bank_detail: {
  //       bank_name: this.thirdFormGroup.value.bankName,
  //       bank_ifsc_code: this.thirdFormGroup.value.ifscCode,
  //       account_number: this.thirdFormGroup.value.accountNo,
  //       account_holder_name: this.thirdFormGroup.value.accountHolderName
  //     },
  //     shop_address: this.adharProofImage,
  //     gstno: this.secondFormGroup.value.city,
  //     aadhar_card_front_img: this.adharImage,
  //     aadhar_card_back_img: this.adharBackImage,
  //     location: {
  //       city: this.secondFormGroup.value.gstNo,
  //       state: this.stateButton,
  //       pincode: this.pincodeButton
  //     },
  //     ip_address: this.IPAddress
  //   }
   

    
  // }

  finalSubmit(){
  let  payload = 
    {
      mobileno: this.firstFormGroup.value.mobile,
      emailid: this.firstFormGroup.value.email,
      vendor_name: this.firstFormGroup.value.vendorName,
      shop_name: this.firstFormGroup.value.shopName,
      password: this.firstFormGroup.value.password,
      // shop_address:"someth",
      gstno: this.secondFormGroup.value.gstNo,
        location: {
            city: this.secondFormGroup.value.city,
            state: this.secondFormGroup.value.state,
            pincode: this.pincodeButton
        },
        aadhar_card: {
            aadhar_card_number: this.fourthFormGroup.value.adharCardNumber,
            DOB: this.fourthFormGroup.value.adharCardDOB,
            aadhar_card_name: this.fourthFormGroup.value.adharName
        },
        bank_detail: {
          bank_name: this.thirdFormGroup.value.bankName,
          bank_ifsc_code: this.thirdFormGroup.value.ifscCode,
          account_number: this.thirdFormGroup.value.accountNo,
          account_holder_name: this.thirdFormGroup.value.accountHolderName
        },
    
        cancel_check_img: this.checkImage,
        address_proof_img:this.adharProofImage,
        aadhar_card_front_img:this.adharImage,
        aadhar_card_back_img:this.adharBackImage,
      
      // shop_cover_img:{
      //        "original": "customer/originalSize/Capture11.PNG",
      //       "originalURL": "https://otmline.s3.ap-south-1.amazonaws.com/customer/originalSize/Capture11.PNG",
      //       "reduce": "customer/reduceSize/Capture11.PNG",
      //       "reduceURL": "https://otmline.s3.ap-south-1.amazonaws.com/customer/reduceSize/Capture11.PNG",
      //       "medium": "customer/mediumSize/Capture11.PNG",
      //       "mediumURL": "https://otmline.s3.ap-south-1.amazonaws.com/customer/mediumSize/Capture11.PNG",
      //       "small": "customer/smallSize/Capture11.PNG",
      //       "smallURL": "https://otmline.s3.ap-south-1.amazonaws.com/customer/smallSize/Capture11.PNG"
      //   },
      vendor_profile_img:this.profileImage
      
    }
    this.http.postData(ApiUrl.register, payload)
    .subscribe(res => {
      if (res.success_code == 200) {
        this.toastr.success(res.message, 'success', {
          timeOut: 2000
        });
        this._router.navigate(['/login']);
      }
    },
      () => {
      });

  }

}


  