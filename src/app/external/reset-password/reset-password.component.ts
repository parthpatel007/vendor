import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import { ForgotPasswordSendlinkComponent } from 'src/app/popup/forgot-password-sendlink/forgot-password-sendlink.component';
import { RefillFormComponent } from 'src/app/popup/refill-form/refill-form.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from 'src/app/services/http.service';
import { ToastrService } from 'ngx-toastr';
import { MustMatch } from 'src/app/_helpers/must-match.validator';
import { ApiUrl } from 'src/app/services/apiurl';
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  forgotForm: any = FormGroup
  submitted = false;
  button = 'Change Password';
  isLoading = false;
  mobile:any
  password :string = 'password';
  password1:string = 'password';
  disablePassword:boolean = true;

  constructor(public dialog: MatDialog,private formBuilder: FormBuilder, private _router: Router, public http: HttpService, private toastr: ToastrService,private _route: ActivatedRoute) { 
    this.forgotForm = this.formBuilder.group({
      otp: ['', Validators.required],
      password: ['', Validators.required],
      confirmpassword: ['', Validators.required]
    }, {
      validator: MustMatch('password', 'confirmpassword')
  });
  }

  ngOnInit(): void {
    this._route.queryParams.subscribe(params => {
      this.mobile = params['mobile'];
    });
  }
  onClickHide(){
    if (this.password === 'password') {
      this.password = 'text';
    } else {
      this.password = 'password';
    }
  }
  onClickHide1(){
    if (this.password1 === 'password') {
      this.password1 = 'text';
    } else {
      this.password1 = 'password';
    }
  }

  keyup(event: any) {
    var length = `${event.target.value}`.length
    if(length===4){
      let payload = {
        mobileno: this.mobile,
        otp: event.target.value,
      }
      this.http.postData(ApiUrl.verifyOtp, payload)
      .subscribe(res => {
        if (res.success_code == 200) {
         this.disablePassword=false
          this.toastr.success(res.message, 'success', {
            timeOut: 2000
          });
        }
      },
        () => {

          this.isLoading = false;
          this.button = 'Change Password';

        });
    }

}

  get f() { return this.forgotForm.controls; }

  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.forgotForm.invalid) {
      return;
    }

    this.isLoading = true;
    this.button = 'Processing';

    let payload = {
      mobileno: this.mobile,
      newPassword: this.forgotForm.value.password,
    }

    this.http.postData(ApiUrl.resetPassword, payload)
      .subscribe(res => {
        if (res.success_code == 200) {
         this.disablePassword=false
         this.isLoading = false;
         this.button = 'Change Password';
          this.toastr.success(res.message, 'success', {
            timeOut: 2000
          });
          this.openDialog()
        }
      },
        () => {

          this.isLoading = false;
          this.button = 'Change Password';

        });
    }

  

  keyDownFunction(event: any) {
    if (event.keyCode === 13) {
      this.onSubmit();
    }
  }

  // --------dialog-main----------//
  openDialog(): void {
    const dialogRef = this.dialog.open(ForgotPasswordSendlinkComponent,  {
      panelClass: 'comman-small-size-modal'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
