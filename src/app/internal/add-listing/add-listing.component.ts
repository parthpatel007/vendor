import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddVariationsComponent } from 'src/app/popup/add-variations/add-variations.component';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ImageProcessingService } from 'src/app/services/image-processing-service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpService } from 'src/app/services/http.service';
import { ApiUrl } from 'src/app/services/apiurl';
import { LinkPhotosComponent } from 'src/app/popup/link-photos/link-photos.component';

@Component({
  selector: 'app-add-listing',
  templateUrl: './add-listing.component.html',
  styleUrls: ['./add-listing.component.scss']
})
export class AddListingComponent implements OnInit {
  allImagesArray: any = []
  submitted = false;
  productForm: any = []
  productImage: any = []
  variationData = true
  variantData:any=[]


  constructor(private formBuilder: FormBuilder, public dialog: MatDialog, private ImageService: ImageProcessingService, private toastr: ToastrService, public http: HttpService) {
    this.productForm = this.formBuilder.group({
      title: ['', Validators.required],
      discription: ['', Validators.required],
      category: ['', Validators.required],
      price: ['', Validators.required],
      quantity: ['', Validators.required],
      sku: ['', Validators.required]
    })
  }

  ngOnInit(): void {
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(AddVariationsComponent, {
      panelClass: 'comman-slice-big-size-modal'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.variantData=result.data
      console.log(this.variantData)
    });
  }

  linkPhotos(){

    const dialogRef = this.dialog.open(LinkPhotosComponent, {
      panelClass: 'comman-slice-big-size-modal'
    });

    dialogRef.afterClosed().subscribe(result => {

    });

  }

  images: any = [
    {
      img: '../../../assets/images/add-photo-icon.svg',
      name: 'Add a photo',
      photo: true,
      default: true,
      length: 9
    },
    {
      img: '../../../assets/images/first-angle.svg',
      name: 'Primary Photo',
      default: true
    },
    {
      img: '../../../assets/images/second-angle.svg',
      name: 'Every angle',
      default: true
    },
    {
      img: '../../../assets/images/third-angle.svg',
      name: 'Every angle',
      default: true
    },
    {
      img: '../../../assets/images/fourth-angle.svg',
      name: 'Every angle',
      default: true
    },
    {
      img: '../../../assets/images/fifth-angle.svg',
      name: 'Details',
      default: true
    },
    {
      img: '../../../assets/images/sixth-angle.svg',
      name: 'In use',
      default: true
    },
    {
      img: '../../../assets/images/seventh-angle.svg',
      name: 'Size & Scale',
      default: true
    },
    {
      img: '../../../assets/images/eight-angle.svg',
      name: 'Styled scene',
      default: true
    },
    {
      img: '../../../assets/images/nineth-angle.svg',
      name: 'Variations',
      default: true
    },
  ];

  names: any = [
    // {
    //   img: '../../../assets/images/add-photo-icon.svg',
    //   name: 'Add a photo',
    //   photo: true,
    //   default: true
    // },
    {
      img: '../../../assets/images/first-angle.svg',
      name: 'Primary Photo',
      default: true
    },
    {
      img: '../../../assets/images/second-angle.svg',
      name: 'Every angle',
      default: true
    },
    {
      img: '../../../assets/images/third-angle.svg',
      name: 'Every angle',
      default: true
    },
    {
      img: '../../../assets/images/fourth-angle.svg',
      name: 'Every angle',
      default: true
    },
    {
      img: '../../../assets/images/fifth-angle.svg',
      name: 'Details',
      default: true
    },
    {
      img: '../../../assets/images/sixth-angle.svg',
      name: 'In use',
      default: true
    },
    {
      img: '../../../assets/images/seventh-angle.svg',
      name: 'Size & Scale',
      default: true
    },
    {
      img: '../../../assets/images/eight-angle.svg',
      name: 'Styled scene',
      default: true
    },
    {
      img: '../../../assets/images/nineth-angle.svg',
      name: 'Variations',
      default: true
    },
  ];

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.images, event.previousIndex, event.currentIndex);
  }

  imageUpload(event: any) {

    if (event) {
      if (this.allImagesArray.length <= 8) {
        this.ImageService.imageFile().then((image: File) => {
          if (image.size >= 5000000) {
            this.toastr.error('File too Big, please select a file less than 5mb', 'error', {
              timeOut: 2000
            })
            return
          }
          var form_data = new FormData();

          form_data.append('img', image);

          this.http.postData(ApiUrl.vendorImageUpload, form_data)
            .subscribe(res => {
              if (res.success_code == 200) {
                this.productImage.push(res.data)
                console.log(this.productImage)
                this.toastr.success(res.message, 'success', {
                  timeOut: 2000
                });
              }
            },
              () => {
              });
          var reader = new FileReader();
          reader.readAsDataURL(image);
          reader.onload = (_event) => {
            let imgs = {
              img: reader.result,
            }

            this.allImagesArray.push(imgs)

            this.images.splice(this.allImagesArray.length, 1);
            this.images.splice(this.allImagesArray.length - 1, 0, imgs);
          }

        })
      }
    }
  }



  array_move(index: any) {
    this.images.splice(index, 1);
    this.allImagesArray.splice(index, 1);
    this.images.splice(this.allImagesArray.length + 1, 0, this.names[this.allImagesArray.length]);
  };


  get f() { return this.productForm.controls; }

  onSubmit() {

    this.submitted = true;
    // stop here if form is invalid
    if (this.productForm.invalid) {
      return;
    }
  }

  cancel() {
    this.productForm.reset()
  }

  handleChange(name: any) {
    console.log(name)
    if (name == 'Simple') {
      this.variationData = false
    } else if (name == 'Variation') {
      this.variationData = true
    }
  }


}
