import { NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import { AuthGuardService as AuthGuard } from 'src/app/services/authguard.service';
import { AddListingComponent } from './add-listing.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { ReactiveFormsModule } from '@angular/forms';


const routes: Routes = [
    {
        path: '', children: [
            {
                path: '',
                component: AddListingComponent,
                canActivate: [AuthGuard],
                data: {title: 'AddListing'},
            }
        ]
    }
];

@NgModule({
    declarations: [
      AddListingComponent,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MatCheckboxModule,
        MatRadioModule,
        MatSlideToggleModule,
        BsDatepickerModule.forRoot(),
        DragDropModule,
        ReactiveFormsModule
    ],
    
})
export class AddListingModule { }
