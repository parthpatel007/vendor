import { NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import { AuthGuardService as AuthGuard } from 'src/app/services/authguard.service';
import { FollowerManagementComponent } from './follower-management.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';


const routes: Routes = [
    {
        path: '', children: [
            {
                path: '',
                component: FollowerManagementComponent,
                canActivate: [AuthGuard],
                data: {title: 'FollowerManagement'},
            }
        ]
    }
];

@NgModule({
    declarations: [
      FollowerManagementComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MatCheckboxModule,
        MatRadioModule,
        MatSlideToggleModule,
        BsDatepickerModule.forRoot(),
    ],
    
})
export class FollowerManagementModule { }
