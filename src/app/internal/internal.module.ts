import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { InternalComponent } from './internal.component';
import { SidebarComponent } from '../base/sidebar/sidebar.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

const routes: Routes = [{
  path: '', component: InternalComponent,
  children: [
    {
      path: 'dashboard',
      loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
    },
    {
      path: 'listing',
       loadChildren: () => import('./listing/listing.module').then(m => m.ListingModule)
    },
    {
      path: 'add-listing',
      loadChildren: () => import('./add-listing/add-listing.module').then(m => m.AddListingModule)
    },
    {
      path: 'order-management',
      loadChildren: () => import('./order-management/order-management.module').then(m => m.OrderManagementModule)
    },
    {
      path: 'reseller-name',
      loadChildren: () => import('./reseller-name/reseller-name.module').then(m => m.ResellerNameModule)
    },
    {
      path: 'payment-management',
      loadChildren: () => import('./payment-managment/payment-managment.module').then(m => m.PaymentManagmentModule)
    },
    {
      path: 'otm-line',
      loadChildren: () => import('./otm-line/otm-line.module').then(m => m.OtmLineModule)
    },
    {
      path: 'follower-management',
      loadChildren: () => import('./follower-management/follower-management.module').then(m => m.FollowerManagementModule)
    },
    {
      path: 'message',
      loadChildren: () => import('./message/message.module').then(m => m.MessageModule)
    },
    {
      path: 'profile',
      loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule)
    }

  ]
}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatCheckboxModule,
    MatRadioModule,
    MatSlideToggleModule,
    BsDatepickerModule.forRoot(),

    

  ],
  exports: [],
  declarations: [
    InternalComponent,
    SidebarComponent,
  ]
})
export class InternalModule {
  constructor() {
  }
}
