import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AllOrderDetailsComponent } from 'src/app/popup/all-order-details/all-order-details.component';
import { CancelOrderComponent } from 'src/app/popup/cancel-order/cancel-order.component';

@Component({
  selector: 'app-reseller-name',
  templateUrl: './reseller-name.component.html',
  styleUrls: ['./reseller-name.component.scss']
})
export class ResellerNameComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(AllOrderDetailsComponent,  {
      panelClass: 'comman-extra-big-size-modal'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openDialogs(): void {
    const dialogRef = this.dialog.open(CancelOrderComponent,  {
      panelClass: 'cancel-order-dialog'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
