import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-add-variations',
  templateUrl: './add-variations.component.html',
  styleUrls: ['./add-variations.component.scss']
})
export class AddVariationsComponent implements OnInit {

  variationTypeButton: any = 'Choose variation type'
  myControl = new FormControl();
  myControl1 = new FormControl();
  options: string[] = ['Pink', 'Blue', 'Red'];
  options1: string[] = ['S', 'M', 'L'];
  filteredOptions: Observable<string[]>;
  filteredOptions1: Observable<string[]>;
  color: any = []
  size: any = []
  filterName: string;
  filterName1: string;
  item:any=['Color','Size']
  colorFlag=false
  sizeFlag=false
  addPrice:any
  addQuantity:any
  sizePrice:any
  sizeQuantity:any
  colorPrice:any
  colorQuantity:any
  array1:any=[]
  array2:any=[]


  constructor(public dialogRef: MatDialogRef<AddVariationsComponent>) { }

  ngOnInit(): void {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );

      this.filteredOptions1 = this.myControl1.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter1(value))
      );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  public _filter1(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options1.filter(option => option.toLowerCase().includes(filterValue));
  }


  variationType(name: any) {
    this.variationTypeButton = name
    if(name=='Color'){
     this.colorFlag=true
     this.item.splice(this.item.findIndex((e:any) => e === name), 1);
    }else if(name=='Size'){
      this.sizeFlag=true
      this.item.splice(this.item.findIndex((e:any) => e === name), 1);
    }
  }

  addColor() {
    if (this.myControl.value != undefined && this.myControl.value != null && this.myControl.value != '') {
      this.color.push(this.myControl.value)
      this.filterName = '';
      this.options.splice(this.options.findIndex(e => e === this.myControl.value), 1);
    }
  }

  addSize() {
    if (this.myControl1.value != undefined && this.myControl1.value != null && this.myControl1.value != '') {
      this.size.push(this.myControl1.value)
      this.filterName1 = '';
      this.options1.splice(this.options1.findIndex(e => e === this.myControl1.value), 1);
    }
  }

  deleteChip(i: any, name: any) {
    this.color.splice(i, 1);
    this.options.push(name)
  }

  deleteSize(i: any, name: any) {
    this.size.splice(i, 1);
    this.options1.push(name)
  }

  deletePrimaryColor(name:any) {
 if(name=='Color'){
    this.colorFlag=false
    this.item.push(name)
    this.variationTypeButton = 'Choose variation type'
    this.options = ['Pink', 'Blue', 'Red'];
    this.color = []
 }else if(name=='Size'){
  this.sizeFlag=false
  this.item.push(name)
  this.variationTypeButton = 'Choose variation type'
  this.options1 = ['S', 'M', 'L'];
  this.size = []
 }
  }

  showOptions(event:MatCheckboxChange,name:any): void {

    if(name=='price'){
      this.sizePrice=event.checked
      
    }else if(name=='quantity'){
      this.sizeQuantity=event.checked

    }  
}


showOptions1(event:MatCheckboxChange,name:any): void {

  if(name=='price'){
    this.colorPrice=event.checked
    
  }else if(name=='quantity'){
    this.colorQuantity=event.checked

  }  
}

closeDialog(): void {

  for (let index = 0; index < this.color.length; index++) {
    const element = this.color[index];

    let obj = {
      colorName:element
    }
    this.array1.push(obj)
  }

for (let index = 0; index < this.size.length; index++) {
  const element = this.size[index];

  let obj = {
    sizeName:element
  }
  this.array2.push(obj)
}
let arr3 = this.array1.map((item:any, i:any) => Object.assign({}, item, this.array2[i]));

  const myDataObject = {
   data:arr3
    }
   this.dialogRef.close(myDataObject);
}

}
