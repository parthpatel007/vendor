import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password-sendlink',
  templateUrl: './forgot-password-sendlink.component.html',
  styleUrls: ['./forgot-password-sendlink.component.scss']
})
export class ForgotPasswordSendlinkComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ForgotPasswordSendlinkComponent>,private _router: Router) { }

  ngOnInit(): void {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  login(){
    this._router.navigate(['/login']);
    this.dialogRef.close();
  }
}
