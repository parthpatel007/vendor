export const ApiUrl = {
    login:  'v1/vendor/auth/vendor_login',
    register:  'v1/vendor/auth/vendor_register',
    sendOtp: 'v1/vendor/auth/send_otp',
    verifyOtp: 'v1/vendor/auth/verify_otp',
    resetPassword: 'v1/vendor/auth/reset_password',
    varifyMobile: 'v1/vendor/auth/verify_mobile',
    pincode: 'v1/vendor/auth/get_pincode',
    getState: 'http://www.postalpincode.in/api/pincode',
    uploadImage: 'v1/vendor/auth/vendor_img_upload',
    getIP: 'https://geolocation-db.com/json',
    vendorImageUpload: 'v1/vendor/listing/upload_product_img'
};

