import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';
import { DOCUMENT } from '@angular/common';
import { FormBuilder } from '@angular/forms';


declare var $: any;


@Injectable()
export class HttpService {

    public filesData: any = [];
    modalRefArr: any = [];
    public readonly apiEndpoint: String;
    private loaderSubject = new BehaviorSubject<any>(null);
    public loaderStatus = this.loaderSubject.asObservable();

    private contactUpdatedSubject = new BehaviorSubject<any>(null);
    public contactUpdatedStatus = this.contactUpdatedSubject.asObservable();

    public eventSubject = new BehaviorSubject<any>(null);
    public eventStatus = this.eventSubject.asObservable();

    private modalSubject = new BehaviorSubject<any>(null);
    public modalStatus = this.modalSubject.asObservable();

    private searchSubject = new BehaviorSubject<any>(null);
    public searchStatus = this.searchSubject.asObservable();

   
    loginData: any;
    myLoader = false;

    headers = new HttpHeaders({ "Content-Type": "application/json" });
  
    options = { headers: this.headers, withCredintials: true };

    constructor(
        private router: Router, public http: HttpClient, public toastr: ToastrService,
        @Inject(DOCUMENT) public document: any, public fb: FormBuilder,
    ) {
        this.apiEndpoint = environment.apiBaseUrl;
    }

    isFormValid(form:any) {
        if (form.valid) {
            return true;
        } else {
            Object.keys(form.controls).forEach(key => {
                form.controls[key].markAsTouched({ onlySelf: true });
            });
            return false;
        }
    }

    postData(url:any, payload:any) {
        return this.http.post<any>(this.apiEndpoint + url, payload, this.options);
    }

    getData(url:any) {
        return this.http.get<any>(this.apiEndpoint + url, this.options);
    }

    getIp(url:any) {
        return this.http.get<any>(url,this.options);
    }

    getStateByPincode(url:any,pincode:any) {
        return this.http.get<any>(url + '/' + pincode,this.options);
    }

   
}

